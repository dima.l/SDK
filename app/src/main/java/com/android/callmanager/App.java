package com.android.callmanager;


import android.app.Application;

import com.app.agrinstappaker.Instappaker;
import com.flurry.android.FlurryAgent;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        new FlurryAgent.Builder().build(this, Instappaker.FLURRY_ID);
    }
}
