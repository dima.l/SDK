
package com.app.agrinstappaker.Model;

import java.util.List;
import com.google.gson.annotations.SerializedName;


public class Response {

    @SerializedName("apps")
    private List<AppsModel> mApps;
    @SerializedName("interval")
    private int mInterval;
    @SerializedName("redirect")
    private String mRedirect;
    @SerializedName("start_time")
    private int mStartTime;

    public List<AppsModel> getApps() {
        return mApps;
    }

    public void setApps(List<AppsModel> apps) {
        mApps = apps;
    }

    public int getInterval() {
        return mInterval;
    }

    public void setInterval(int interval) {
        mInterval = interval;
    }

    public String getRedirect() {
        return mRedirect;
    }

    public void setRedirect(String redirect) {
        mRedirect = redirect;
    }

    public int getStartTime() {
        return mStartTime;
    }

    public void setStartTime(int startTime) {
        mStartTime = startTime;
    }

}
