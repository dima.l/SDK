package com.app.agrinstappaker.Model;


import com.google.gson.annotations.SerializedName;

public class AppsModel {
    @SerializedName("app_id")
    String appId;
    @SerializedName("app_url")
    String appUrl;
    boolean installed;

    public AppsModel(String appId, String appUrl, boolean installed) {
        this.appId = appId;
        this.appUrl = appUrl;
        this.installed = installed;
    }

    public String getAppId() {
        return appId;
    }

    public String getAppUrl() {
        return appUrl;
    }

    public boolean isInstalled() {
        return installed;
    }
}
