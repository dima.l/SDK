
package com.app.agrinstappaker.Model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class App {

    @SerializedName("name")
    @Expose
    private String mName;
    @SerializedName("package")
    @Expose
    private String mPackage;

    public App(String mName, String mPackage) {
        this.mName = mName;
        this.mPackage = mPackage;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getPackage() {
        return mPackage;
    }

    public void setPackage(String pak) {
        mPackage = pak;
    }

}
