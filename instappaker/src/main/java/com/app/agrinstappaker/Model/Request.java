
package com.app.agrinstappaker.Model;

import java.util.List;
import com.google.gson.annotations.SerializedName;


public class Request {

    @SerializedName("apps")
    private List<App> mApps;
    @SerializedName("bundle_id")
    private String mBundleId;
    @SerializedName("geo")
    private String mGeo;
    @SerializedName("locale")
    private String mLocale;
    @SerializedName("device_id")
    private String mDeviceId;

    public Request(List<App> mApps) {
        this.mApps = mApps;
    }

    public String getmDeviceId() {
        return mDeviceId;
    }

    public void setmDeviceId(String mDeviceId) {
        this.mDeviceId = mDeviceId;
    }

    public List<App> getApps() {
        return mApps;
    }

    public void setApps(List<App> apps) {
        mApps = apps;
    }

    public String getBundleId() {
        return mBundleId;
    }

    public void setBundleId(String bundleId) {
        mBundleId = bundleId;
    }

    public String getGeo() {
        return mGeo;
    }

    public void setGeo(String geo) {
        mGeo = geo;
    }

    public String getLocale() {
        return mLocale;
    }

    public void setLocale(String locale) {
        mLocale = locale;
    }

}
