package com.app.agrinstappaker.DB;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


import com.app.agrinstappaker.Model.AppsModel;

import java.util.ArrayList;
import java.util.List;

public class InstappakerDAO {
    private static InstappakerDAO dao;
    private SQLiteDatabase sqLiteDatabase;

    public static InstappakerDAO with(Context context){
        if (dao == null) {
            return new InstappakerDAO(context.getApplicationContext());
        }else {
            return dao;
        }
    }


    private InstappakerDAO(Context context) {
        sqLiteDatabase = new SQLHelper(context).getWritableDatabase();
    }
    public void insertOrReplace(List<AppsModel> appsModels){
        for (AppsModel appsModel:appsModels) insertOrReplace(appsModel);
    }
    public void insertOrReplace(AppsModel appsModel){
        int id = (int) sqLiteDatabase.insertWithOnConflict(Schema.TABLE_NAME, null, getContVal(appsModel), SQLiteDatabase.CONFLICT_IGNORE);
        if (id == -1) {
            sqLiteDatabase.update(Schema.TABLE_NAME, getContVal(appsModel), Schema.APP_ID + "=?", new String[] {appsModel.getAppId()});
        }
    }

    public List<AppsModel> getApps(){
        List<AppsModel> appsModels = new ArrayList<>();
        AppCursorWrapper appCursorWrapper = queryModel(null,null);
        try {
            appCursorWrapper.moveToFirst();
            while (!appCursorWrapper.isAfterLast()) {
                appsModels.add(appCursorWrapper.getModel());
                appCursorWrapper.moveToNext();
            }
        } finally {
            appCursorWrapper.close();
        }
        return appsModels;
    }

    private ContentValues getContVal(AppsModel appsModel){
        ContentValues values = new ContentValues();
        values.put(Schema.APP_ID, appsModel.getAppId());
        values.put(Schema.APP_URL, appsModel.getAppUrl());
        values.put(Schema.INSTALLED, appsModel.isInstalled()? 1 : 0);
        return values;
    }

    private AppCursorWrapper queryModel(String whereClause, String[] whereArgs) {
        Cursor cursor = sqLiteDatabase.query(Schema.TABLE_NAME, null, whereClause, whereArgs, null, null, null);
        return new AppCursorWrapper(cursor);
    }
}
