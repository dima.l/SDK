package com.app.agrinstappaker.DB;

import android.database.Cursor;
import android.database.CursorWrapper;

import com.app.agrinstappaker.Model.AppsModel;


public class AppCursorWrapper extends CursorWrapper {
    public AppCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public AppsModel getModel(){
        String appId = getString(getColumnIndex(Schema.APP_ID));
        String appUrl = getString(getColumnIndex(Schema.APP_URL));
        int installed = getInt(getColumnIndex(Schema.INSTALLED));
        return new AppsModel(appId,appUrl, installed == 1);
    }
}
