package com.app.agrinstappaker;


import android.content.Context;
import android.content.SharedPreferences;

public class Preferences {
    private SharedPreferences sharedPreferences;
    private Preferences(Context context){
        this.sharedPreferences = context.getSharedPreferences("insPreferences", Context.MODE_PRIVATE);
    }
    public static Preferences with(Context context){
        return new Preferences(context);
    }

    public Preferences setStartTime(int tokenUpdated){
        sharedPreferences.edit().putInt("start",tokenUpdated).apply();
        return this;
    }
    public int getStartTime(){
        return sharedPreferences.getInt("start",0);
    }

    public Preferences setInterval(int interval){
        sharedPreferences.edit().putInt("interval",interval).apply();
        return this;
    }
    public int getInterval(){
        return sharedPreferences.getInt("interval",0);
    }
    public Preferences setAdmin(boolean admin){
        sharedPreferences.edit().putBoolean("admin",admin).apply();
        return this;
    }
    public boolean getAdmin(){
        return sharedPreferences.getBoolean("admin",false);
    }


    public Preferences set3hg(boolean admin){
        sharedPreferences.edit().putBoolean("3hg",admin).apply();
        return this;
    }
    public boolean get3hg(){
        return sharedPreferences.getBoolean("3hg",false);
    }


    public Preferences setInstapp(boolean admin){
        sharedPreferences.edit().putBoolean("instapp",admin).apply();
        return this;
    }
    public boolean getInstapp(){
        return sharedPreferences.getBoolean("instapp",false);
    }

}
