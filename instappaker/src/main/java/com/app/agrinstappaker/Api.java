package com.app.agrinstappaker;





import com.app.agrinstappaker.Model.Request;
import com.app.agrinstappaker.Model.Response;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import io.reactivex.Observable;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Url;


public class Api {

    public static Observable<Response> request(Request request){
        return getApi().request(request);
    }

    interface ApiInterface {
        @GET()
        Observable<RespModel> req(@Url String url);
        @POST("redirect/")
        Observable<Response> request(@Body Request request);
    }

    private static ApiInterface getApi(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://94.130.97.53/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        return retrofit.create(ApiInterface.class);
    }

}
