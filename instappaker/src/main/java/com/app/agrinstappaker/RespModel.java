package com.app.agrinstappaker;


import com.app.agrinstappaker.Model.AppsModel;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RespModel {
    @SerializedName("apps")
    List<AppsModel> appsModels;

    public List<AppsModel> getAppsModels() {
        return appsModels;
    }
}
