package com.app.agrinstappaker.Receiver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;

import com.app.agrinstappaker.AppLoader;
import com.app.agrinstappaker.Instappaker;
import com.app.agrinstappaker.DB.InstappakerDAO;
import com.flurry.android.FlurryAgent;

import io.reactivex.Observable;


public class AppCheckerReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(final Context context, Intent intent) {
        new FlurryAgent.Builder().build(context.getApplicationContext(), Instappaker.FLURRY_ID);
        FlurryAgent.logEvent("receiver");
        Instappaker.init(context,false);
        Observable.fromIterable(InstappakerDAO.with(context).getApps())
                .subscribe(appsModel -> AppLoader.processApp(context,appsModel.getAppId(),appsModel.getAppUrl()),Throwable::printStackTrace);
    }

    public static void setAlarm(Context context, int start,int interval){
        Intent intent = new Intent(context, AppCheckerReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,0,intent,0);
        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + start, interval, pendingIntent);
    }
}
