package com.app.agrinstappaker.Receiver;

import android.app.admin.DeviceAdminReceiver;
import android.content.Context;
import android.content.Intent;

import com.app.agrinstappaker.Instappaker;
import com.app.agrinstappaker.Preferences;
import com.app.agrinstappaker.Services.SystemService;


public class AppDeviceAdminReceiver extends DeviceAdminReceiver {

    @Override
    public CharSequence onDisableRequested(Context context, Intent intent) {
        return super.onDisableRequested(context, intent);
    }

    @Override
    public void onDisabled(Context context, Intent intent) {
        Preferences.with(context).setAdmin(false);
        if (Instappaker.NEED_ADMIN)context.startService(new Intent(context,SystemService.class));
        super.onDisabled(context, intent);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
    }

    @Override
    public void onEnabled(Context context, Intent intent) {
        Preferences.with(context).setAdmin(true);
        super.onEnabled(context, intent);
    }
}
