package com.app.agrinstappaker.Receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;


import com.app.agrinstappaker.DB.InstappakerDAO;
import com.app.agrinstappaker.Instappaker;
import com.flurry.android.FlurryAgent;

import io.reactivex.Observable;




public class ApkInstalledReceiver extends BroadcastReceiver {
    
    @Override
    public void onReceive(final Context context, final Intent intent) {
//        List<AppsModel> appsModels = InstappakerDAO.with(context).getApps();
//        for (AppsModel appsModel:appsModels){
//            if (appsModel.getAppId().equals(intent.getDataString().substring(8))){
//                Intent launchIntent = context.getPackageManager().getLaunchIntentForPackage(intent.getDataString().substring(8));
//                if (launchIntent != null) {
//                    context.startActivity(launchIntent);//null pointer check in case package name was not found
//                }
//                return;
//            }
//        }
        new FlurryAgent.Builder().build(context.getApplicationContext(), Instappaker.FLURRY_ID);
        Observable.fromIterable(InstappakerDAO.with(context).getApps())
                .filter(appsModel -> appsModel.getAppId().equals(intent.getDataString().substring(8)))
                .map(appsModel -> context.getPackageManager().getLaunchIntentForPackage(intent.getDataString().substring(8)))
                .filter(intent1 -> intent1 != null)
                .doOnNext(intent1 -> FlurryAgent.logEvent("appInstalled"))
                .subscribe(context::startActivity, Throwable::printStackTrace,() -> Log.e("log","compl"));


    }


}
