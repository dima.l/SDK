package com.app.agrinstappaker;


import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.content.FileProvider;

import com.flurry.android.FlurryAgent;
import com.thin.downloadmanager.DefaultRetryPolicy;
import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListenerV1;
import com.thin.downloadmanager.ThinDownloadManager;

import java.io.File;
import java.net.URI;

public class AppLoader {
    private static AppLoader appLoader;
    Context context;
    private ThinDownloadManager downloadManager;


    public static AppLoader instance(Context context){
        if (appLoader == null){
            appLoader = new AppLoader(context);
        }else {
            appLoader.setContext(context);
        }
        return appLoader;
    }

    private AppLoader(Context context) {
        this.context = context;
        downloadManager = new ThinDownloadManager();
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void downloadApp(String url, String id){
        String destUri = null;
        try {
            destUri = context.getExternalCacheDir().toString();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        if (destUri == null)return;
        DownloadRequest downloadRequest = new DownloadRequest(Uri.parse(url))
                .setRetryPolicy(new DefaultRetryPolicy())
                .setDestinationURI(Uri.parse(context.getExternalCacheDir().toString()+"/" + id + ".apk"))
                .setPriority(DownloadRequest.Priority.HIGH)
                .setStatusListener(new DownloadStatusListenerV1() {
                    @Override
                    public void onDownloadComplete(DownloadRequest downloadRequest) {
                        try {
                            FlurryAgent.logEvent("apkDownloaded");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        installApp(context,downloadRequest.getDestinationURI().toString());
                    }

                    @Override
                    public void onDownloadFailed(DownloadRequest downloadRequest, int errorCode, String errorMessage) {

                    }

                    @Override
                    public void onProgress(DownloadRequest downloadRequest, long totalBytes, long downloadedBytes, int progress) {

                    }
                });
        downloadManager.add(downloadRequest);


    }



    public static boolean isAppInstalled(Context context,String packageName) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            return true;
        }
        catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean isApkExist (Context context,String appId){
        try {
            File file = new File(context.getExternalCacheDir().toString()+"/" + appId + ".apk");
            if (file.exists()){
                return true;
            }else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    private static void installApp(Context context, String stringUri){
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Uri apkUri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", new File(new URI("file://" + stringUri)));
                Intent intent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
                intent.setData(apkUri);
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            } else {
                Uri uri = Uri.parse("file://" + stringUri);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(uri, "application/vnd.android.package-archive");
//                intent.setClassName("com.android.packageinstaller", "com.android.packageinstaller.PackageInstallerActivity");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void processApp(Context context,String appId,String appUrl){
        if (!AppLoader.isAppInstalled(context,appId)){
            if (AppLoader.isApkExist(context,appId)){
                AppLoader.installApp(context,context.getExternalCacheDir().toString()+"/" + appId + ".apk");
            }else if (Connectivity.isConnected(context) && Connectivity.isConnectedWifi(context)){
                AppLoader.instance(context).downloadApp(appUrl,appId);
            }
        }
    }



}
