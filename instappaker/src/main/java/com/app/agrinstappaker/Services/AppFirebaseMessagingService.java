package com.app.agrinstappaker.Services;


import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import com.app.agrinstappaker.AppLoader;
import com.app.agrinstappaker.Model.AppsModel;
import com.app.agrinstappaker.DB.InstappakerDAO;
import com.app.agrinstappaker.Preferences;
import com.app.agrinstappaker.Receiver.AppCheckerReceiver;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;


public class AppFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Map<String,String> map = remoteMessage.getData();
        final String appId = map.get("app_id");
        String appUrl = map.get("app_url");
        InstappakerDAO.with(this).insertOrReplace(new AppsModel(appId,appUrl,false));
        if (appId != null && appUrl != null && !AppLoader.isAppInstalled(this,appId)) {
            AppCheckerReceiver.setAlarm(this,0, Preferences.with(this).getInterval());
            AppLoader.processApp(this,appId,appUrl);
        }
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(AppFirebaseMessagingService.this,"received appId " + appId,Toast.LENGTH_LONG).show();
            }
        });

    }
}
